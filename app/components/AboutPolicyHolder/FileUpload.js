import React from "react";
import { Form } from 'react-bootstrap';
import { useForm } from "react-hook-form";

function FileUpload() {
  const { register } = useForm()
 return (
    <Form.Group >
      
      <Form.Label>Please Upload Driver's License</Form.Label><br />
      <Form.Control type="file" {...register('file', { required: true })} />

    </Form.Group>
  );
}

export default FileUpload;