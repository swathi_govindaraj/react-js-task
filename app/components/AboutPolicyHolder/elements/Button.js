import React, { useContext, useEffect, useState } from 'react';
import { Form, Button} from 'react-bootstrap';
import { AddressContext } from '../FormContext';
import '../App.css';
import AddDriver from './AddDriver';

function ButtonInput({field_id, field_label, field_placeholder, field_value, field_mandatory,  errors}){

    const { loadAddress } = useContext(AddressContext);

    const [showAddDriverButton, setShowAddDriverButton]= useState(true);
    const [showAddDriver, setShowAddDriver]= useState(false);

    
    useEffect(() => {
        const postalCode =document.getElementById("Postal Code_Id");
        const AddressLine1 =document.getElementById("Address1_Id");
        const AddressLine2 =document.getElementById("Address2_Id");
        
        document.getElementById("findAddress_Id").onclick = function () {
            if(postalCode.value && AddressLine1.value && AddressLine2.value ){
                 loadAddress();
            }
        }

        document.getElementById("add_driver").onclick = function () {
            setShowAddDriver(true);
            setShowAddDriverButton(false);
        }  
    }, []);

    const showButton = ()=>{
        setShowAddDriverButton(true);
        setShowAddDriver(false);
    }

      return (
            <>
            <Button style={{display: ( showAddDriverButton ? 'block' : 'none')}} className="button-rounded" id={field_id}>{field_label}</Button>

            <div style={{display: ( showAddDriver ? 'block' : 'none')}}> 
                <AddDriver action={showButton}/>
            </div>
            </>
      );
}

export default ButtonInput;