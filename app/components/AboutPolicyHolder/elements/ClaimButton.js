import React, { useContext, useEffect, useState } from 'react';
import { Form, Button} from 'react-bootstrap';
import { FormContext } from '../FormContext';
import AddClaimTable from './AddClaimTable';

function ClaimButton({field_id, field_label, field_placeholder, field_value, field_mandatory, conviction_table_fields, errors}){

    const { handleChange } = useContext(FormContext)

    const [showClaimButton, setShowClaimButton] = useState(true);
    const [showClaimTable, setShowClaimTable] = useState(false);

    useEffect(()=>{
        const claim_button = document.getElementById("claim_button");
        claim_button.onclick = function () {
            if (conviction_button.name == "button") {
                setShowClaimTable(true);
                setShowClaimButton(false);
            }
            }
    },[])

    const showButton = ()=>{
        setShowClaimTable(false);
        setShowClaimButton(true);
    }
    
    return (
        <>
        <Button style={{display: ( showClaimButton ? 'block' : 'none')}} className="button-rounded" name="button" id="claim_button" >
            {field_label}
        </Button>

        <div style={{display: ( showClaimTable ? 'block' : 'none')}}> 
            <AddClaimTable action={showButton}/>
        </div>
        </>
      );
}

export default ClaimButton;