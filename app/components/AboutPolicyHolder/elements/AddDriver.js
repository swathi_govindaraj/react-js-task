import React from 'react';
import { Form, Button, Row, Col, Table} from 'react-bootstrap';

class AddDriver extends React.Component {
    
  state = {
    drivers_details: [{
        driver_title: "",
        driver_first_name: "",
        driver_last_name: "",
        driver_gender: "",
        driver_dob: "",
        driver_marital_status: "",
        driver_country: "",
        driver_citizen_by_birth: "",
        driver_contact_number: "",
        driver_email: "",
        driver_post_code: "",
        driver_address_line_1: "",
        driver_address_line_2: "",
        driver_city_or_town: "",
        driver_country: "",
        policy_holder_occupation_type: "",
        policy_holder_occupation: "",
        policy_holder_industry: "",
        policy_holder_other_occupation: "",
        driver_license: "",
        driver_license_number: "",
        driver_license_period: "",
        driver_license_cancelled: "",
        driver_license_number1: "",
        driver_dvla_medical_conditions: "",
        driver_medical_conditions: "",
        driver_accidents_claims: "",
        errors: [{
          driver_title: "",
          driver_first_name: "",
          driver_last_name: "",
          driver_gender: "",
          driver_dob: "",
          driver_marital_status: "",
          driver_country: "",
          driver_citizen_by_birth: "",
          driver_contact_number: "",
          driver_email: "",
          driver_post_code: "",
          driver_address_line_1: "",
          driver_address_line_2: "",
          driver_city_or_town: "",
          driver_country: "",
          policy_holder_occupation_type: "",
          policy_holder_occupation: "",
          policy_holder_industry: "",
          policy_holder_other_occupation: "",
          driver_license: "",
          driver_license_number: "",
          driver_license_period: "",
          driver_license_cancelled: "",
          driver_license_number1: "",
          driver_dvla_medical_conditions: "",
          driver_medical_conditions: "",
          driver_accidents_claims: ""
        }],
        claim_details: [{
          claim_details: "",
          claim_date: "",
          claim_amount: "",
          claim_type: "",
          errors: [{
            claim_details: "",
            claim_date: "",
            claim_amount: "",
            claim_type: ""
          }]
        }],
          conviction_details: [{
            conviction_date: "",
            dvla_code: "",
            points_incurred: "",
            fine_incurred: "",
            ban_length: "",
            driver_breathlaysed: "",
            breathalyser_reading: "",
            offense_type: "",
            offense_type: "",
            errors: [{
              conviction_date: "",
              dvla_code: "",
              points_incurred: "",
              fine_incurred: "",
              ban_length: "",
              driver_breathlaysed: "",
              breathalyser_reading: "",
              offense_type: "",
              offense_type: ""
            }]
          }]
    }],

    driver_title_options: ["Mr","Mrs","Dr","Miss","Ms"],
    driver_gender_options: ["Male", "Female"],
    driver_marital_status_options: ["Single","Married"],
    driver_country_options:["US","UK"],
    policy_holder_occupation_options: ["Option 1","Option 2"],
    driver_license_options: ["Full UK License","Provisional UK License","Other/None"],
    driver_license_period_options: ["Up to 1 year","Up to x years","Over 20 years"],
    driver_medical_conditions_options: ["Option 1","Option 2"],

    claim_type_options: ["Option 1", "Option 2"],

    yes_or_no_options: ["Yes","No"],

    showClaimButton: false,
    showClaimTable: false,
    showConvictionButton: true,
    showConvictionTable: false
  };

  showClaimTable = e => {
      this.setState({
        showClaimTable: true
      });
      this.setState({
        showClaimButton: false
      }); 
  }

  showConvictionTable = e => {
    this.setState({
      showConvictionTable: true
    });
    this.setState({
      showConvictionButton: false
    });  
}

  handleDriverDetailsChange = idx => e => {
    const { name, value } = e.target;
    const drivers_details = [...this.state.drivers_details];

    if(name === "driver_first_name"){
      drivers_details[idx].errors[0].driver_first_name = "";
        if(value.match(/^[a-zA-Z\s]*$/)){
          drivers_details[idx][name] = value; 
        }
        else drivers_details[idx].errors[0].driver_first_name = "Letters Only"
    }

    if(name === "driver_last_name" && value !== ""){
      drivers_details[idx].errors[0].driver_last_name = "";
      if(value.match(/^[a-zA-Z\s]+$/)){
        drivers_details[idx][name] = value;
      }
      else drivers_details[idx].errors[0].driver_last_name = "Letters Only"
    }

    if(name === "driver_contact_number" && value !== ""){
      drivers_details[idx].errors[0].driver_contact_number = "";
      if(value.length === 10){
        drivers_details[idx][name] = value;
      }
      else drivers_details[idx].errors[0].driver_contact_number = "Contact Number should be 10 digits"
    }

    if(name === "driver_email" && value !== ""){
      drivers_details[idx].errors[0].driver_email = "";
      if(value.match("^(.+)@(.+)$")){
        drivers_details[idx][name] = value;
      }
      else drivers_details[idx].errors[0].driver_email = "Please enter a valid email address"
    }

    if(name === "driver_post_code" && value !== ""){
      drivers_details[idx].errors[0].driver_post_code = "";
      if(value.length === 6){
        drivers_details[idx][name] = value;
      }
      else drivers_details[idx].errors[0].driver_post_code = "Post Code should be 6 digits"
    }

    drivers_details[idx][name] = value;

    this.setState({
      drivers_details
    });
    console.log(this.state.drivers_details);
  };

  handleClaimDetailsChange = idx => e => {
  
    const { name, value } = e.target;
    const drivers_details = [...this.state.drivers_details];

    drivers_details[idx].claim_details[0][name] = value;

    this.setState({
      drivers_details
    });
    console.log(drivers_details);
  };

  handleConvictionDetailsChange = idx => e => {
    const { name, value } = e.target;
    const drivers_details = [...this.state.drivers_details];
    drivers_details[idx].conviction_details[0][name] = value;

    this.setState({
      drivers_details
    });
    console.log(drivers_details);
  };

  handleAddRow = (e) => {
    e.preventDefault();
    const item = {
        driver_title: "",
        driver_first_name: "",
        driver_last_name: "",
        driver_gender: "",
        driver_dob: "",
        driver_marital_status: "",
        driver_country: "",
        driver_citizen_by_birth: "",
        driver_contact_number: "",
        driver_email: "",
        driver_post_code: "",
        driver_address_line_1: "",
        driver_address_line_2: "",
        driver_city_or_town: "",
        driver_country: "",
        policy_holder_occupation_type: "",
        policy_holder_occupation: "",
        policy_holder_industry: "",
        policy_holder_other_occupation: "",
        driver_license: "",
        driver_license_number: "",
        driver_license_period: "",
        driver_license_cancelled: "",
        driver_license_number1: "",
        driver_dvla_medical_conditions: "",
        driver_medical_conditions: "",
        driver_accidents_claims: "",
        errors: [{
          driver_title: "",
          driver_first_name: "",
          driver_last_name: "",
          driver_gender: "",
          driver_dob: "",
          driver_marital_status: "",
          driver_country: "",
          driver_citizen_by_birth: "",
          driver_contact_number: "",
          driver_email: "",
          driver_post_code: "",
          driver_address_line_1: "",
          driver_address_line_2: "",
          driver_city_or_town: "",
          driver_country: "",
          policy_holder_occupation_type: "",
          policy_holder_occupation: "",
          policy_holder_industry: "",
          policy_holder_other_occupation: "",
          driver_license: "",
          driver_license_number: "",
          driver_license_period: "",
          driver_license_cancelled: "",
          driver_license_number1: "",
          driver_dvla_medical_conditions: "",
          driver_medical_conditions: "",
          driver_accidents_claims: ""
        }],
        claim_details: [{
          claim_details: "",
          claim_date: "",
          claim_amount: "",
          claim_type: "",
          errors: [{
            claim_details: "",
            claim_date: "",
            claim_amount: "",
            claim_type: ""
          }]
        }],
        conviction_details: [{
          conviction_date: "",
          dvla_code: "",
          points_incurred: "",
          fine_incurred: "",
          ban_length: "",
          driver_breathlaysed: "",
          breathalyser_reading: "",
          offense_type: "",
          offense_type: "",
          errors: [{
            conviction_date: "",
            dvla_code: "",
            points_incurred: "",
            fine_incurred: "",
            ban_length: "",
            driver_breathlaysed: "",
            breathalyser_reading: "",
            offense_type: "",
            offense_type: ""
          }]
      }]
        
    };
    this.setState({
      drivers_details: [...this.state.drivers_details, item]
    });
  };

  handleClaimTableAddRow = idx => e => {
    e.preventDefault();
    console.log("Inside Add Claim");
    console.log(this.state.drivers_details[idx].claim_details);
    const item = {
        claim_details: "",
        claim_date: "",
        claim_amount: "",
        claim_type: "",
        errors: [{
          claim_details: "",
          claim_date: "",
          claim_amount: "",
          claim_type: ""
        }]
    };
    
    this.setState({
      drivers_details: [...this.state.drivers_details[idx].claim_details, item]
    });
    console.log(this.state.drivers_details);
  };

  handleConvictionTableAddRow = idx => e  => {
    e.preventDefault();
    console.log("Inside Add Conviction");
    console.log(this.state.drivers_details[idx].conviction_details);
    const item = {
        conviction_date: "",
        dvla_code: "",
        points_incurred: "",
        fine_incurred: "",
        ban_length: "",
        driver_breathlaysed: "",
        breathalyser_reading: "",
        offense_type: "",
        offense_type: "",
        errors: [{
          conviction_date: "",
          dvla_code: "",
          points_incurred: "",
          fine_incurred: "",
          ban_length: "",
          driver_breathlaysed: "",
          breathalyser_reading: "",
          offense_type: "",
          offense_type: ""
        }]
    };
    this.setState({
      drivers_details: [...this.state.drivers_details[idx].conviction_details, item]
    });
  };

  handleRemoveSpecificRow = (idx) => (e) => {
    e.preventDefault();
    const drivers_details = [...this.state.drivers_details]
    if(drivers_details.length === 1){
        const drivers_details = [...this.state.drivers_details];
        drivers_details[idx] = {
            driver_title: null,
            driver_first_name: null,
            driver_last_name: null,
            driver_gender: null,
            driver_dob: null,
            driver_marital_status: null,
            driver_country: null,
            driver_citizen_by_birth: null,
            driver_contact_number: null,
            driver_email: null,
            driver_post_code: null,
            driver_address_line_1: null,
            driver_address_line_2: null,
            driver_city_or_town: null,
            driver_country: null,
            policy_holder_occupation_type: null,
            policy_holder_occupation: null,
            policy_holder_industry: null,
            policy_holder_other_occupation: null,
            driver_license: null,
            driver_license_number: null,
            driver_license_period: null,
            driver_license_cancelled: null,
            driver_license_number1: null,
            driver_dvla_medical_conditions: null,
            driver_medical_conditions: null,
            driver_accidents_claims: null,
            errors: [{
              driver_title: null,
              driver_first_name: null,
              driver_last_name: null,
              driver_gender: null,
              driver_dob: null,
              driver_marital_status: null,
              driver_country: null,
              driver_citizen_by_birth: null,
              driver_contact_number: null,
              driver_email: null,
              driver_post_code: null,
              driver_address_line_1: null,
              driver_address_line_2: null,
              driver_city_or_town: null,
              driver_country: null,
              policy_holder_occupation_type: null,
              policy_holder_occupation: null,
              policy_holder_industry: null,
              policy_holder_other_occupation: null,
              driver_license: null,
              driver_license_number: null,
              driver_license_period: null,
              driver_license_cancelled: null,
              driver_license_number1: null,
              driver_dvla_medical_conditions: null,
              driver_medical_conditions: null,
              driver_accidents_claims: null
            }],
            claim_details: [{
              claim_details: null,
              claim_date: null,
              claim_amount: null,
              claim_type: null,
              errors: [{
                claim_details: null,
                claim_date: null,
                claim_amount: null,
                claim_type: null
              }]
            }],
            conviction_details: [{
              conviction_date: null,
              dvla_code: null,
              points_incurred: null,
              fine_incurred: null,
              ban_length: null,
              driver_breathlaysed: null,
              breathalyser_reading: null,
              offense_type: null,
              offense_type: null,
              errors: [{
                conviction_date: null,
                dvla_code: null,
                points_incurred: null,
                fine_incurred: null,
                ban_length: null,
                driver_breathlaysed: null,
                breathalyser_reading: null,
                offense_type: null,
                offense_type: null
              }]
          }]
        };
        this.setState({
        drivers_details
        });

        const showButton = this.props.action;
        showButton();
    }
    else{
        drivers_details.splice(idx, 1);
        this.setState({ drivers_details });
    }
  }

  radioButtonCheck = (e) => {
    
    [...document.getElementsByClassName("radio-button-input")].forEach(node => {
            node.checked === true
              ? (node.parentElement.classList.add("radio-button-checked"))
              : (node.parentElement.classList.remove("radio-button-checked"));
          });

        if(e.target.id === 'driver_accidents_claims_Yes') {
          this.setState({
            showClaimButton: true
          }); 
        }
        else {
          this.setState({
            showClaimButton: false
          });
          this.setState({
            showClaimTable: false
          });
        };

}

  render() {
    return (
      <div>
        <h2>About Driver</h2>
        {this.state.drivers_details.map((item, idx) => (
          <React.Fragment key={idx}>
          <Form.Group>
            <Form.Label>Driver's Title</Form.Label>
            <Form.Control className="form-field " name="driver_title" as="select" value={this.state.drivers_details[idx].driver_title} onChange={this.handleDriverDetailsChange(idx)}>
            <option >-- Please Select --</option>
                {this.state.driver_title_options.length > 0 && this.state.driver_title_options.map((option, i) =>
                    <option value={option} key={i}>{option}</option>
                )}
                </Form.Control>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_title ? this.state.drivers_details[idx].errors[0].driver_title : ""}</span>
        </Form.Group>
          
          <Row>
          <Col>
            <Form.Group>
                <Form.Label>Driver's First Name</Form.Label>
                <Form.Control className="form-field" name="driver_first_name"  value={this.state.drivers_details[idx].driver_first_name} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_first_name ? this.state.drivers_details[idx].errors[0].driver_first_name : ""}</span>
            </Form.Group>
          </Col>
          <Col>
            <Form.Group>
                <Form.Label>Driver's Last Name</Form.Label>
                <Form.Control className="form-field" name="driver_last_name"  value={this.state.drivers_details[idx].driver_last_name} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_last_name ? this.state.drivers_details[idx].errors[0].driver_last_name : ""}</span>
            </Form.Group>
          </Col>
          </Row>

          <Row>
          <Col>
          <Form.Group>
            <Form.Label id="radio-button-label">Driver's Gender</Form.Label>
            <Row>
                {this.state.driver_gender_options.length > 0 && this.state.driver_gender_options.map((option, i) =>
                <Col key={i} lg={5}>
                    <Form.Group key={i} id="radio-button-field" className="radio-button" onChange={this.radioButtonCheck} >
                        <input id={option} name="driver_gender" className="radio-button-input" type="radio" value={option} checked={this.state.drivers_details[idx].driver_gender === option} onChange={this.handleDriverDetailsChange(idx)} />
                        {' '}{' '}
                    <Form.Label>{option}</Form.Label>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_gender ? this.state.drivers_details[idx].errors[0].driver_gender : ""}</span>
                    </Form.Group>
                </Col>
            )}
            </Row>
            </Form.Group>
            </Col>
            <Col>

            <Form.Group>
              <Form.Label>Driver's Date of Birth</Form.Label>
              <Form.Control className="form-field" name="driver_dob" type="date" value={this.state.drivers_details[idx].driver_dob} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_dob ? this.state.drivers_details[idx].errors[0].driver_dob : ""}</span>
            </Form.Group>
            </Col>
            </Row>

            <Row>
            <Col>
            <Form.Group>
            <Form.Label>Driver's Marital Status</Form.Label>
            <Form.Control className="form-field " name="driver_marital_status" as="select" value={this.state.drivers_details[idx].driver_marital_status} onChange={this.handleDriverDetailsChange(idx)}>
            <option >-- Please Select --</option>
                {this.state.driver_marital_status_options.length > 0 && this.state.driver_marital_status_options.map((option, i) =>
                    <option value={option} key={i}>{option}</option>
                )}
                </Form.Control>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_marital_status ? this.state.drivers_details[idx].errors[0].driver_marital_status : ""}</span>
            </Form.Group>
            </Col>
            <Col>
            <Form.Group>
            <Form.Label>Driver is a permanent citizen of</Form.Label>
            <Form.Control className="form-field " name="driver_country" as="select" value={this.state.drivers_details[idx].driver_country} onChange={this.handleDriverDetailsChange(idx)}>
            <option >-- Please Select --</option>
                {this.state.driver_country_options.length > 0 && this.state.driver_country_options.map((option, i) =>
                    <option value={option} key={i}>{option}</option>
                )}
                </Form.Control>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_country ? this.state.drivers_details[idx].errors[0].driver_country : ""}</span>
            </Form.Group>
            </Col>
            </Row>

            <Row>
            <Col>
            <Form.Group>
            <Form.Label id="radio-button-label">Is the Driver a citizen by birth?</Form.Label>
            <Row>
                {this.state.yes_or_no_options.length > 0 && this.state.yes_or_no_options.map((option, i) =>
                <Col key={i} lg={5}>
                    <Form.Group key={i} id="radio-button-field" className="radio-button" onChange={this.radioButtonCheck} >
                        <input id={option} name="driver_citizen_by_birth" className="radio-button-input" type="radio" value={option} checked={this.state.drivers_details[idx].driver_citizen_by_birth === option} onChange={this.handleDriverDetailsChange(idx)} />
                        {' '}{' '}
                    <Form.Label>{option}</Form.Label>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_citizen_by_birth ? this.state.drivers_details[idx].errors[0].driver_citizen_by_birth : ""}</span>
                    </Form.Group>
                </Col>
            )}
            </Row>
            </Form.Group>
            </Col>
           <Col>
            <Form.Group>
                <Form.Label>Driver's contact number</Form.Label>
                <Form.Control className="form-field" name="driver_contact_number"  value={this.state.drivers_details[idx].driver_contact_number} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_contact_number ? this.state.drivers_details[idx].errors[0].driver_contact_number : ""}</span>
            </Form.Group>
            </Col>
            </Row>


            <Row>
              <Col>
            <Form.Group>
                <Form.Label>Driver's Email</Form.Label>
                <Form.Control className="form-field" name="driver_email"  value={this.state.drivers_details[idx].driver_email} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_email ? this.state.drivers_details[idx].errors[0].driver_email : ""}</span>
            </Form.Group>
            </Col>
            <Col>

            <Form.Group>
                <Form.Label>Driver's Post Code</Form.Label>
                <Form.Control className="form-field" name="driver_post_code"  value={this.state.drivers_details[idx].driver_post_code} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_post_code ? this.state.drivers_details[idx].errors[0].driver_post_code : ""}</span>
            </Form.Group>
            </Col>
            </Row>

            <Button className="btn-sm">
             Find Address
            </Button>

            <Row>
          <Col>
            <Form.Group>
                <Form.Label>Address Line 1</Form.Label>
                <Form.Control className="form-field" name="driver_address_line_1"  value={this.state.drivers_details[idx].driver_address_line_1} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_address_line_1 ? this.state.drivers_details[idx].errors[0].driver_address_line_1 : ""}</span>
            </Form.Group>
            </Col>
            <Col>
            <Form.Group>
                <Form.Label>Address Line 2</Form.Label>
                <Form.Control className="form-field" name="driver_address_line_2"  value={this.state.drivers_details[idx].driver_address_line_2} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_address_line_2 ? this.state.drivers_details[idx].errors[0].driver_address_line_2 : ""}</span>
            </Form.Group>
            </Col>
            </Row>

            <Row>
            <Col>
            <Form.Group>
                <Form.Label>Town/City</Form.Label>
                <Form.Control className="form-field" name="driver_city_or_town"  value={this.state.drivers_details[idx].driver_city_or_town} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_city_or_town ? this.state.drivers_details[idx].errors[0].driver_city_or_town : ""}</span>
            </Form.Group>
            </Col>
            <Col>

            <Form.Group>
                <Form.Label>Country</Form.Label>
                <Form.Control className="form-field" name="driver_country"  value={this.state.drivers_details[idx].driver_country} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_country ? this.state.drivers_details[idx].errors[0].driver_country : ""}</span>
            </Form.Group>
            </Col>
            </Row>
            
            <Row>
              <Col>
            <Form.Group>
            <Form.Label>Policy holder's occupation type</Form.Label>
            <Form.Control className="form-field " name="policy_holder_occupation_type" as="select" value={this.state.drivers_details[idx].policy_holder_occupation_type} onChange={this.handleDriverDetailsChange(idx)}>
            <option >-- Please Select --</option>
                {this.state.policy_holder_occupation_options.length > 0 && this.state.policy_holder_occupation_options.map((option, i) =>
                    <option value={option} key={i}>{option}</option>
                )}
                </Form.Control>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].policy_holder_occupation_type ? this.state.drivers_details[idx].errors[0].policy_holder_occupation_type : ""}</span>
            </Form.Group>
            </Col>
            <Col>
            <Form.Group>
                <Form.Label>Policy Holder's Occupation</Form.Label>
                <Form.Control className="form-field" name="policy_holder_occupation"  value={this.state.drivers_details[idx].policy_holder_occupation} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].policy_holder_occupation ? this.state.drivers_details[idx].errors[0].policy_holder_occupation : ""}</span>
            </Form.Group>
            </Col>
            </Row>
            
            <Row>
              <Col>
            <Form.Group>
                <Form.Label>Enter the industry where the policy holder is occupied in</Form.Label>
                <Form.Control className="form-field" name="policy_holder_industry"  value={this.state.drivers_details[idx].policy_holder_industry} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].policy_holder_industry ? this.state.drivers_details[idx].errors[0].policy_holder_industry : ""}</span>
            </Form.Group>
            </Col>
            <Col>
            <Form.Group>
            <Form.Label id="radio-button-label">Does the policy holder have any other occupation?</Form.Label>
            <Row>
                {this.state.yes_or_no_options.length > 0 && this.state.yes_or_no_options.map((option, i) =>
                <Col key={i} lg={5}>
                    <Form.Group key={i} id="radio-button-field" className="radio-button" onChange={this.radioButtonCheck} >
                        <input id={option} name="policy_holder_other_occupation" className="radio-button-input" type="radio" value={option} checked={this.state.drivers_details[idx].policy_holder_other_occupation === option} onChange={this.handleDriverDetailsChange(idx)} />
                        {' '}{' '}
                    <Form.Label>{option}</Form.Label>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].policy_holder_other_occupation ? this.state.drivers_details[idx].errors[0].policy_holder_other_occupation : ""}</span>
                    </Form.Group>
                </Col>
            )}
            </Row>
            </Form.Group>
            </Col>
            </Row>
            
            <Row>
              <Col>
            <Form.Group>
            <Form.Label>Driver's license type</Form.Label>
            <Form.Control className="form-field " name="driver_license" as="select" value={this.state.drivers_details[idx].driver_license} onChange={this.handleDriverDetailsChange(idx)}>
            <option >-- Please Select --</option>
                {this.state.driver_license_options.length > 0 && this.state.driver_license_options.map((option, i) =>
                    <option value={option} key={i}>{option}</option>
                )}
                </Form.Control>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_license ? this.state.drivers_details[idx].errors[0].driver_license : ""}</span>
            </Form.Group>
            </Col>

          <Col>
            <Form.Group>
                <Form.Label>Would you like to enter the Driver's driving license number?</Form.Label>
                <Form.Control className="form-field" name="driver_license_number"  value={this.state.drivers_details[idx].driver_license_number} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_license_number ? this.state.drivers_details[idx].errors[0].driver_license_number : ""}</span>
            </Form.Group>
            </Col>
            </Row>

            <Row>
              <Col>
            <Form.Group>
            <Form.Label>Select how long has the Driver held this license</Form.Label>
            <Form.Control className="form-field " name="driver_license_period" as="select" value={this.state.drivers_details[idx].driver_license_period} onChange={this.handleDriverDetailsChange(idx)}>
            <option >-- Please Select --</option>
                {this.state.driver_license_period_options.length > 0 && this.state.driver_license_period_options.map((option, i) =>
                    <option value={option} key={i}>{option}</option>
                )}
                </Form.Control>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_license_period ? this.state.drivers_details[idx].errors[0].driver_license_period : ""}</span>
        </Form.Group>
        </Col>

        <Col>
        <Form.Group>
            <Form.Label id="radio-button-label">Have you ever had license cancelled/suspended or declared void?</Form.Label>
            <Row>
                {this.state.yes_or_no_options.length > 0 && this.state.yes_or_no_options.map((option, i) =>
                <Col key={i} lg={5}>
                    <Form.Group key={i} id="radio-button-field" className="radio-button" onChange={this.radioButtonCheck} >
                        <input id={option} name="driver_license_cancelled" className="radio-button-input" type="radio" value={option} checked={this.state.drivers_details[idx].driver_license_cancelled === option} onChange={this.handleDriverDetailsChange(idx)} />
                        {' '}{' '}
                    <Form.Label>{option}</Form.Label>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_license_cancelled ? this.state.drivers_details[idx].errors[0].driver_license_cancelled : ""}</span>
                    </Form.Group>
                </Col>
            )}
            </Row>
            </Form.Group>
            </Col>
            </Row>

            <Row>
              <Col>
            <Form.Group>
                <Form.Label>Driver's driving license number</Form.Label>
                <Form.Control className="form-field" name="driver_license_number1"  value={this.state.drivers_details[idx].driver_license_number1} onChange={this.handleDriverDetailsChange(idx)}/>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_license_number1 ? this.state.drivers_details[idx].errors[0].driver_license_number1 : ""}</span>
            </Form.Group>
            </Col>

            <Col>
            <Form.Group>
            <Form.Label id="radio-button-label">Does the Driver have any DVLA-reported medical conditions or disabilities?</Form.Label>
            <Row>
                {this.state.yes_or_no_options.length > 0 && this.state.yes_or_no_options.map((option, i) =>
                <Col key={i} lg={5}>
                    <Form.Group key={i} id="radio-button-field" className="radio-button" onChange={this.radioButtonCheck} >
                        <input id={option} name="driver_dvla_medical_conditions" className="radio-button-input" type="radio" value={option} checked={this.state.drivers_details[idx].driver_dvla_medical_conditions === option} onChange={this.handleDriverDetailsChange(idx)} />
                        {' '}{' '}
                    <Form.Label>{option}</Form.Label>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_dvla_medical_conditions ? this.state.drivers_details[idx].errors[0].driver_dvla_medical_conditions : ""}</span>
                    </Form.Group>
                </Col>
            )}
            </Row>
            </Form.Group>
            </Col>
            </Row>
            
            <Row>
              <Col>
            <Form.Group>
            <Form.Label>Select the medical conditions or disabilities</Form.Label>
            <Form.Control className="form-field " name="driver_medical_conditions" as="select" value={this.state.drivers_details[idx].driver_medical_conditions} onChange={this.handleDriverDetailsChange(idx)}>
            <option >-- Please Select --</option>
                {this.state.driver_medical_conditions_options.length > 0 && this.state.driver_medical_conditions_options.map((option, i) =>
                    <option value={option} key={i}>{option}</option>
                )}
                </Form.Control>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_medical_conditions ? this.state.drivers_details[idx].errors[0].driver_medical_conditions : ""}</span>
            </Form.Group>
            </Col>
            <Col>
            <Form.Group>
            <Form.Label id="radio-button-label">Does the Driver have any motor accidents or claims in the last 5 years?</Form.Label>
            <Row>
                {this.state.yes_or_no_options.length > 0 && this.state.yes_or_no_options.map((option, i) =>
                <Col key={i} lg={5}>
                    <Form.Group key={i} id="radio-button-field" className="radio-button" onChange={this.radioButtonCheck} >
                        <input id={`driver_accidents_claims_${option}`} name="driver_accidents_claims" className="radio-button-input" type="radio" value={option} checked={this.state.drivers_details[idx].driver_accidents_claims === option} onChange={this.handleDriverDetailsChange(idx)} />
                        {' '}{' '}
                    <Form.Label>{option}</Form.Label>
            <span style={{color: "red"}}>{this.state.drivers_details[idx].errors[0].driver_accidents_claims ? this.state.drivers_details[idx].errors[0].driver_accidents_claims : ""}</span>
                    </Form.Group>
                </Col>
            )}
            </Row>
            <Button style={{display: ( this.state.showClaimButton ? 'block' : 'none')}} onClick={this.showClaimTable} className="button-rounded" name="button" id="claim_button" >
              Add Claim
            </Button>
            </Form.Group>
            </Col>
            </Row>

            <Button style={{display: ( this.state.showConvictionButton ? 'block' : 'none')}} onClick={this.showConvictionTable} className="button-rounded" name="button" id="conviction_button" >
              Add Conviction
            </Button>


          <Button variant="danger" className="button-rounded btn-sm" onClick={this.handleRemoveSpecificRow(idx)}>
            Remove
          </Button>
             {' '}         
          <Button variant="info" onClick={this.handleAddRow} className="button-rounded btn-sm">
            Add Another Driver
          </Button>

          {/* Claim Table */}
          <div style={{display: ( this.state.showClaimTable ? 'block' : 'none')}}>
          <Table responsive striped bordered hover>
                <thead>
                  <tr>
                    <th className="text-center" style={{fontSize: "17px"}}> Claim Details</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Claim Date</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Claim Amount</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Claim Type</th>
                    <th/>
                    <th/>
                  </tr>
                </thead>
                <tbody>
                    <tr key={idx}>
                      <td>
                      <Form.Control 
                        id="claim_details_0"
                        type="text" 
                        name="claim_details"
                        className="form-field form-control" 
                        value={this.state.drivers_details[idx].claim_details[0].claim_details}
                        onChange={this.handleClaimDetailsChange(idx)}
                        />
                    </td>
                    <td>
                    <Form.Control
                        id="claim_date_0"
                        type="date" 
                        name="claim_date"
                        className="form-field form-control"
                        value={this.state.drivers_details[idx].claim_details[0].claim_date}
                        onChange={this.handleClaimDetailsChange(idx)}
                        format="dd-mm-yyyy" 
                        />
                    </td>
                    <td>
                        <Form.Control 
                        id="claim_amount_0"
                        type="number" 
                        name="claim_amount"
                        className="form-field form-control" 
                        value={this.state.drivers_details[idx].claim_details[0].claim_amount}
                        onChange={this.handleClaimDetailsChange(idx)}
                        />
                    </td>
                    <td>
                      <Form.Control id="claim_type_0" className="form-field form-control" name="claim_type" as="select" value={this.state.drivers_details[idx].claim_details[0].claim_type}  onChange={this.handleClaimDetailsChange(idx)}>
                      <option >Please Select</option>
                          {this.state.claim_type_options.length > 0 && this.state.claim_type_options.map((option, i) =>
                              <option value={option} key={i}>{option}</option>
                          )}
                      </Form.Control>
                        
                    </td>
                    <td>
                        {/* <Button variant="danger" className="button-rounded btn-sm" onClick={this.handleClaimTableRemoveSpecificRow(idx)} >
                          Remove
                        </Button> */}
                    </td>
                    <td>
                        <Button className="button-rounded btn-sm">
                          Edit
                        </Button> 
                    </td>
                    </tr>
                </tbody>
              </Table>
              <Button variant="info" onClick={this.handleClaimTableAddRow(idx)} className="button-rounded btn-sm">
                Add Claim
              </Button>
      </div>

      {/* Conviction Table */}
      <div style={{display: ( this.state.showConvictionTable ? 'block' : 'none')}}>
          <Table responsive striped bordered hover>
                <thead>
                  <tr>
                    <th className="text-center" style={{fontSize: "17px"}}>Conviction Date</th>
                    <th className="text-center" style={{fontSize: "17px"}}>DVLA Code</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Points Incurred</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Fine Incurred</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Ban Length</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Has Driver Breathlaysed</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Breathalyser Reading</th>
                    <th className="text-center" style={{fontSize: "17px"}}>Has Offense Type</th>
                    <th />
                    <th />
                  </tr>
                </thead>
                <tbody>
                    <tr key={idx}>
                      <td>
                        <Form.Control
                        id="conviction_date_0"
                        type="date" 
                        name="conviction_date"
                        className="form-field form-control"
                        value={this.state.drivers_details[idx].conviction_details[0].conviction_date}
                        onChange={this.handleConvictionDetailsChange(idx)}
                        format="dd-mm-yyyy" 
                        />
                    </td>
                    <td>
                        <Form.Control
                        id="dvla_code_0"
                        type="number" 
                        name="dvla_code"
                        className="form-field form-control" 
                        value={this.state.drivers_details[idx].conviction_details[0].dvla_code}
                        onChange={this.handleConvictionDetailsChange(idx)}
                        />
                    </td>
                    <td>
                        <Form.Control 
                        id="points_incurred_0"
                        type="number" 
                        name="points_incurred"
                        className="form-field form-control" 
                        value={this.state.drivers_details[idx].conviction_details[0].points_incurred}
                        onChange={this.handleConvictionDetailsChange(idx)}
                        />
                    </td>
                    <td>
                        <Form.Control
                        id="fine_incurred_0"
                        type="number" 
                        name="fine_incurred"
                        className="form-field form-control" 
                        value={this.state.drivers_details[idx].conviction_details[0].fine_incurred}
                        onChange={this.handleConvictionDetailsChange(idx)}
                        />
                    </td>
                    <td>
                        <Form.Control 
                        id="ban_length_0"
                        type="number" 
                        name="ban_length"
                        className="form-field form-control" 
                        value={this.state.drivers_details[idx].conviction_details[0].ban_length}
                        onChange={this.handleConvictionDetailsChange(idx)}
                        />
                    </td>
                    <td>
                    <Form.Control id="driver_breathlaysed_0" className="form-field form-control" name="driver_breathlaysed" as="select" value={this.state.drivers_details[idx].conviction_details[0].driver_breathlaysed} onChange={this.handleConvictionDetailsChange(idx)}>
                      <option >Please Select</option>
                          {this.state.yes_or_no_options.length > 0 && this.state.yes_or_no_options.map((option, i) =>
                              <option value={option} key={i}>{option}</option>
                          )}
                      </Form.Control>
                        
                    </td>
                    <td>
                        <Form.Control 
                        id="breathalyser_reading_0"
                        type="number" 
                        name="breathalyser_reading"
                        className="form-field form-control" 
                        value={this.state.drivers_details[idx].conviction_details[0].breathalyser_reading}
                        onChange={this.handleConvictionDetailsChange(idx)}
                        />
                    </td>
                    <td>
                      <Form.Control id="offense_type_0" className="form-field form-control" name="offense_type" as="select" value={this.state.drivers_details[idx].conviction_details[0].offense_type}  onChange={this.handleConvictionDetailsChange(idx)}>
                        <option >Please Select</option>
                          {this.state.yes_or_no_options.length > 0 && this.state.yes_or_no_options.map((option, i) =>
                              <option value={option} key={i}>{option}</option>
                          )}
                      </Form.Control>
                        
                    </td>
                    <td>
                        {/* <Button variant="danger" className="button-rounded btn-sm" onClick={this.handleConvictionTableRemoveSpecificRow(idx)}>
                          Remove
                        </Button> */}
                        </td>
                        <td>
                        <Button className="button-rounded btn-sm">
                          Edit
                        </Button>
                        </td>
                  
                    </tr>
                </tbody>
              </Table>
              <Button variant="info" onClick={this.handleConvictionTableAddRow(idx)} className="button-rounded btn-sm">
                Add Conviction
              </Button>
      </div>
          </React.Fragment>
        ))}

      </div>
    );
  }
}

export default AddDriver;