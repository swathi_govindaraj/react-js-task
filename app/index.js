import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {createStore} from 'redux';
import AboutVehicleForm from './components/AboutVehicle/AboutVehicleForm';
import AboutPolicyHolderForm from './components/AboutPolicyHolder/AboutPolicyHolderForm';
import AboutPolicyForm from './components/AboutPolicy/AboutPolicyForm';
import Summary from './components/Summary/Summary';
import PageLoader from './components/AboutVehicle/PageLoader';
import allReducers from './reducers';

const store = createStore(
  allReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

ReactDOM.render(
  <Provider store={store}>
    <AboutVehicleForm />
      {/* <AboutPolicyHolderForm /> */}
      {/* <AboutPolicyForm /> */}
      {/* <Summary /> */}
      <PageLoader/>
  </Provider>,
  document.getElementById('root')
);
